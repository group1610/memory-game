﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace memorygame
{
    /// <summary>
    /// Interaction logic for enregels.xaml
    /// </summary>
    public partial class regels : Window
    {
        public regels()
        {
            InitializeComponent();
        }
        /// <summary>
        /// als je op deze button klikt ga je naar mainmenu
        /// </summary>
        /// <param name="sender">het object waar je op kan klikken</param>
        /// <param name="e"></param>
        private void btnhome(object sender, RoutedEventArgs e)
        {
            MainMenu mw = new MainMenu();
            mw.Show();
            this.Close();
        }
        /// <summary>
        /// als je op deze button klikt ga je naar nlregels.xaml
        /// </summary>
        /// <param name="sender">object waar je op kunt klikken</param>
        /// <param name="e"></param>
        private void btnnl(object sender, RoutedEventArgs e)
        {
            nlregels win = new nlregels();
            win.Show();
            this.Close();
        }
        /// <summary>
        /// als je op deze button klikt ga je naar faregels.xaml
        /// </summary>
        /// <param name="sender">object waar je op kunt klikken</param>
        /// <param name="e"></param>
        private void btnfa(object sender, RoutedEventArgs e)
        {
            faregels win = new faregels();
            win.Show();
            this.Close();
        }
        /// <summary>
        /// als je op deze button klikt ga je naar duregels.xaml
        /// </summary>
        /// <param name="sender">object waar je op kunt klikken</param>
        /// <param name="e"></param>
        private void btndu(object sender, RoutedEventArgs e)
        {
            duregels win = new duregels();
            win.Show();
            this.Close();
        }
        /// <summary>
        /// als je op deze button klikt ga je naar engels.xaml
        /// </summary>
        /// <param name="sender">object waar je op kunt klikken</param>
        /// <param name="e"></param>
        private void btnen(object sender, RoutedEventArgs e)
        {
            enregels win = new enregels();
            win.Show();
            this.Close();
        }
        /// <summary>
        /// als je op deze button klikt sluit het spel
        /// </summary>
        /// <param name="sender">object waar je op kunt klikken</param>
        /// <param name="e"></param>
        private void btnclose(object sender, RoutedEventArgs e)
        {
            
            this.Close();
        }
       
    }
}
