﻿#pragma checksum "..\..\..\regels.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "E62CD797C9E8D787475A8DFD3093E4410BBD4DDF"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using memorygame;


namespace memorygame {
    
    
    /// <summary>
    /// regels
    /// </summary>
    public partial class regels : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 31 "..\..\..\regels.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid background;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\regels.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button duitsbtn;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\regels.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button nederlandsbtn;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\regels.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button fransbtn;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\regels.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button engelsbtn;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.8.1.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Memory Game;component/regels.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\regels.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.8.1.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.background = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.duitsbtn = ((System.Windows.Controls.Button)(target));
            
            #line 35 "..\..\..\regels.xaml"
            this.duitsbtn.Click += new System.Windows.RoutedEventHandler(this.btndu);
            
            #line default
            #line hidden
            return;
            case 3:
            this.nederlandsbtn = ((System.Windows.Controls.Button)(target));
            
            #line 48 "..\..\..\regels.xaml"
            this.nederlandsbtn.Click += new System.Windows.RoutedEventHandler(this.btnnl);
            
            #line default
            #line hidden
            return;
            case 4:
            this.fransbtn = ((System.Windows.Controls.Button)(target));
            
            #line 65 "..\..\..\regels.xaml"
            this.fransbtn.Click += new System.Windows.RoutedEventHandler(this.btnfa);
            
            #line default
            #line hidden
            return;
            case 5:
            this.engelsbtn = ((System.Windows.Controls.Button)(target));
            
            #line 81 "..\..\..\regels.xaml"
            this.engelsbtn.Click += new System.Windows.RoutedEventHandler(this.btnen);
            
            #line default
            #line hidden
            return;
            case 6:
            
            #line 94 "..\..\..\regels.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnhome);
            
            #line default
            #line hidden
            return;
            case 7:
            
            #line 99 "..\..\..\regels.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnclose);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

