﻿using Memory_Game;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace memorygame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainMenu : Window
    {
        public Player player1;
        public Player player2;
        bool existsave = false;
        bool existscore = false;
        string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);


        public MainMenu()
        {
            if (File.Exists(path + "/save.sav"))
             {
                existsave = true;
              }
            if ( File.Exists(path + "/highscore.sav"))
            {
                existscore = true;
            }
            



                InitializeComponent();

        }


        /// <summary>
        /// als je op deze button klikt zorgt dat ervoor dat je naar een xaml gaat waarop de highscores te zien staat
        /// </summary>
        /// <param name="sender">het object waarop geklikt word</param>
        /// <param name="e"></param>
        private void BTNhighscore(object sender, RoutedEventArgs e)
        {
            if (existscore == true)
            {
                highscores win = new highscores();
                win.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Er is nog geen opgeslagen score");
            }


        }
        /// <summary>
        /// als je hier op klikt zorgt dat ervoor dat je het opgeslagen spel word geladen
        /// </summary>
        /// <param name="sender">het object waarop geklikt word</param>
        /// <param name="e"></param>
        private void BTNSpelLaden_Click(object sender, RoutedEventArgs e)
        {
            if (existsave == true)
            {
                MainWindow win = new MainWindow(player1, player2, 1);
                win.Load();
                win.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Er is nog geen opgeslagen spel");
            }

        }
        /// <summary>
        /// als je op deze button klikt start het spel en ga je naar nameinput.xaml daar kun je de namen invoeren van de spelers
        /// </summary>
        /// <param name="sender">object waarop geklikt word</param>
        /// <param name="e"></param>
        private void BTNstartspel_Click(object sender, RoutedEventArgs e)
        {
            NameInput win = new NameInput();
            win.Show();
            this.Close();
        }
        /// <summary>
        /// als je op deze button klikt brengt het je naar regels.xaml en daar kun je kiezen in welke taal je je regels wilt hebben.
        /// </summary>
        /// <param name="sender">het object waarop geklikt kan worden</param>
        /// <param name="e"></param>
        private void Regelsbtn(object sender, RoutedEventArgs e)
        {
            regels win = new regels();
            win.Show();
            this.Close();

        }
        /// <summary>
        /// deze button zorgt ervoor dat het spel sluit
        /// </summary>
        /// <param name="sender">het object waarop geklikt kan worden</param>
        /// <param name="e"></param>
        private void btnclose(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
       


    }

}