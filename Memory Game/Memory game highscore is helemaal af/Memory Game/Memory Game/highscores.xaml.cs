﻿using memorygame;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Memory_Game
{
    /// <summary>
    /// Interaction logic for highscores.xaml
    /// string path zorgt ervoor dat een path is aangemaakt die je naar de desktop kan brengen.
    /// en die gebruik je dan later om de informatie van de highscore uit een .sav bestand te halen.
    /// </summary>
    public partial class highscores : Window
    {
           
        string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        public highscores()
        {
            InitializeComponent();
            Highscores();
            
            
        }
        /// <summary>
        /// als je op deze button klikt zorgt dat ervoor dat het spel sluit
        /// </summary>
        /// <param name="sender">het object waar op geklikt word</param>
        /// <param name="e"></param>
        private void btnclose(object sender, RoutedEventArgs e)
        {

            this.Close();
        }
        /// <summary>
        /// als je op deze button klikt zorgt dat ervoor dat je naar het mainmenu gaat
        /// </summary>
        /// <param name="sender">het object is waar op je kan klikken</param>
        /// <param name="e"></param>
        private void btnhome(object sender, RoutedEventArgs e)
        {
            MainMenu win = new MainMenu();
            win.Show();
            this.Close();
        }
        /// <summary>
        /// hier maak je een method en die method zorgt ervoor dat je de highscore van de top 10 spelers met de hoogste scores kunt zien.
        /// je maakt verschillende list eerst maak je 1 voor de hele line in het bestand highscore.sav 
        /// je maakt gebruik van streamreader om het bestand highscore.sav te lezen. met de variabele path zorgt dat ervoor dat je op de desktop komt.
        /// en daarna maak je voor elk puntje dus de namen, score en datum een aparte list
        /// door de code line.split zorgt dat ervoor dat je verschil kunt maken tussen de namen, de score en de datum. Oftewel je kan ze apart pakken.
        /// je zorgt ervoor dat hij de maximale score boven aan laat zien door gebruik te maken met score.max en indexof 
        /// daarna laat je per regel zien welke namen, score, datum er komt te staan door if == 1 en if ==2 etc
        /// en door index achter de names[index] score[index] en datum[index] te zetten pakt hij dus de juiste volgorde van de hoogste score tot aan de laagste.
        /// dus voor regel 1 oftewel if i == 1 worden de labels die op nummer 1 staan gelijk gezet aan de names, score en datum die op nummer 1 worden gezet in de list. 
        /// en door remove at te doen aan de einde van de code haalt hij hem uit de list zodat je niet twee keer dezelfde ziet staan.
        /// </summary>
        public void Highscores()
        {
            List<string> scorenamen = new List<string>();
            List<string> names = new List<string>();
            List<int> score = new List<int>();
            List<string> datum = new List<string>(); 

            
            using (StreamReader reader = new StreamReader(path + "/highscore.sav"))
            {

                string line;

                while ((line = reader.ReadLine()) != null)
                {

                        scorenamen.Add(line);
                        string[] s = line.Split(';');
                        names.Add(Convert.ToString(s[0]));
                        score.Add(Convert.ToInt32(s[1]));
                        datum.Add(Convert.ToString(s[2]));
                      
                }
                int i = 1;
                while (score.Count > 0)
                {
                    int m = score.Max();
                    int index = score.IndexOf(m);
                    if (i == 1)
                    {
                        player1label.Content = names[index];
                        score1label.Content = score[index];
                        datum1label.Content = datum[index];
                    }
                    
                    if (i == 2)
                    {
                        player2label.Content = names[index];
                        score2label.Content = score[index];
                        datum2label.Content = datum[index];
                    }
                    if (i == 3)
                    {
                        player3label.Content = names[index];
                        score3label.Content = score[index];
                        datum3label.Content = datum[index];
                    }
                    if (i == 4)
                    {
                        player4label.Content = names[index];
                        score4label.Content = score[index];
                        datum4label.Content = datum[index];
                    }
                    if (i == 5)
                    {
                        player5label.Content = names[index];
                        score5label.Content = score[index];
                        datum5label.Content = datum[index];
                    }
                    if (i == 6)
                    {
                        player6label.Content = names[index];
                        score6label.Content = score[index];
                        datum6label.Content = datum[index];
                    }
                    if (i == 7)
                    {
                        player7label.Content = names[index];
                        score7label.Content = score[index];
                        datum7label.Content = datum[index];
                    }
                    if (i == 8)
                    {
                        player8label.Content = names[index];
                        score8label.Content = score[index];
                        datum8label.Content = datum[index];
                    }
                    if (i == 9)
                    {
                        player9label.Content = names[index];
                        score9label.Content = score[index];
                        datum9label.Content = datum[index];
                    }
                    if (i == 10)
                    {
                        player10label.Content = names[index];
                        score10label.Content = score[index];
                        datum10label.Content = datum[index];
                    }
                    i++;
                    names.RemoveAt(index);
                    score.RemoveAt(index);
                    datum.RemoveAt(index);
                }

              

            }
        }
    }
}
