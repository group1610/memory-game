﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace memorygame
{
    /// <summary>
    /// Interaction logic for faregels.xaml
    /// </summary>
    public partial class faregels : Window
    {

        public faregels()
        {
            InitializeComponent();
        }
        /// <summary>
        /// deze button zorgt dat je naar mainmenu gaat als je erop klikt
        /// </summary>
        /// <param name="sender">het object waar je op kan klikken</param>
        /// <param name="e"></param>
        private void btnhome(object sender, RoutedEventArgs e)
        {
            MainMenu win = new MainMenu();
            win.Show();
            this.Close();
        }
        /// <summary>
        /// als je op deze button klikt sluit het het spel
        /// </summary>
        /// <param name="sender">object waar op geklikt kan worden</param>
        /// <param name="e"></param>
        private void btnclose(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// als je op deze button klikt ga je terug naar de vorige pagina dus in dit geval naar regels.xaml
        /// </summary>
        /// <param name="sender">het object waar op geklikt kan worden</param>
        /// <param name="e"></param>
        private void btnback(object sender, RoutedEventArgs e)
        {
            regels win = new regels();
            win.Show();
            this.Close();
        }
    }
}
