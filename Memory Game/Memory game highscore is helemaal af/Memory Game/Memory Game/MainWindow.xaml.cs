﻿using memorygame;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml;

namespace Memory_Game
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        // The object of the first player.
        Player player1;
        
        // The object of the second player.
        Player player2;

        

        private static DispatcherTimer timer = new DispatcherTimer();

        /// <summary>
        /// rows en cols, op 4 gezet zodat er een "grid van 4x4 wordt aangemaakt."
        /// </summary>
        public const int rows = 4;
        public const int cols = 4;
        /// <summary>
        /// Kaartrow1/kaartcol1/kaartcol2/kaartrow2 die ints op 9 gezet zodat als je niet op het eerste kaartje (0,0) kan klikken en dan match hij meteen.
        /// </summary>
        public int kaartrow1 = 9;
        int kaartcol1 = 9;
        int kaartrow2 = 9;
        int kaartcol2 = 9;
        /// <summary>
        /// kaartklik wordt aangemaakt zodat er maar twee kaarten kunnen worden aangeklikt.
        /// </summary>
        int kaartklik = 0;
        /// <summary>
        /// click limiet wordt aangemaakt zodat er later een limiet op kan zetten zodat er maar 2 kunnen aangeklikt worden.
        /// </summary>
        int clicklimiet = 0;
        public int punten = 0;
        public int laden = 0;
        string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

        /// <summary>
        /// isclicked/kaartpleknummer/kaartplaatje/achterkantplaatje. Array aangemaakt voor het opslaan van de plek in de grid.
        /// </summary>
        bool[,] isClicked = new bool[rows, cols];
        public int[,] kaartpleknummer = new int[rows, cols];
        public ImageSource[,] kaartplaatje = new ImageSource[rows, cols];
        public Image[,] achterkantplaatje = new Image[rows, cols];



    

        public MainWindow(Player player1, Player player2,int laden)
        {
            InitializeComponent();
            timer.Interval = TimeSpan.FromSeconds(2);
            timer.Tick += timer_Tick;
            AddImage();
            this.player1 = player1;
            this.player2 = player2;
            this.laden = laden;
            if (laden == 0)
            {
                UpdateLabels(player1, player2);
            }
        }



        /// <summary>
        ///     Update the labels of the players.
        /// </summary>
        /// <param name="player1">The object of the first player.</param>
        /// <param name="player2">The object of the second player.</param>
        public void UpdateLabels(Player player1, Player player2)
        {

            player1NameLabel.Content = player1.GetName();
            player2NameLabel.Content = player2.GetName();

            player1ScoreLabel.Text = player1.GetScore().ToString();
            player2ScoreLabel.Text = player2.GetScore().ToString();

            if (player1.GetTurn() == true)
            {
                player1NameLabel.Foreground = Brushes.Green;
                player2NameLabel.Foreground = Brushes.Black;
            }
            else
            {
                player1NameLabel.Foreground = Brushes.Black;
                player2NameLabel.Foreground = Brushes.Green;
            }
          
        }
        /// <summary>
        /// Hier worden de achterkantjes ingeladen en op de voorkant laten zien zodat je het plaatje nog niet kan zien.
        /// </summary>
        private void AddImage()
        {

            GetImagesList();

            Random random = new Random();



            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    Image ImageOnBacksideOfCard = new Image();
                    ImageOnBacksideOfCard.Source = new BitmapImage(new Uri("afbeeldingen/achterkant.png", UriKind.Relative));
                    achterkantplaatje[row, col] = ImageOnBacksideOfCard;
                    //Ruimte tussen kaartjes
                    Thickness margin = ImageOnBacksideOfCard.Margin;
                    margin.Top = 10;
                    ImageOnBacksideOfCard.Margin = margin;

                    ImageOnBacksideOfCard.MouseDown += new MouseButtonEventHandler(CardClick);




                    ImageOnBacksideOfCard.Tag = new int[2] { row, col };



                    Grid.SetColumn(ImageOnBacksideOfCard, col);
                    Grid.SetRow(ImageOnBacksideOfCard, row);
                    isClicked[row, col] = false;
                    grid.Children.Add(ImageOnBacksideOfCard);


                }
            }
        }

        //Load pictures
        /// <summary>
        /// Hier worden de plaatjes in een lijst gezet van 1/2/3/4/5/6/7/8.png.
        /// En het wordt daarna ook meteen in de kaartplaatje/kaartpleknummer array gezet(op volgorde).
        /// Bij random worden de nummers uit de array van kaartplaatje/kaartpleknummer door elkaar gehusseld.
        /// </summary>
        private void GetImagesList()
        {
            List<ImageSource> images = new List<ImageSource>();

            for (int i = 0; i < 16; i++)
            {
                int imageNr = i % 8 + 1;
                ImageSource source = new BitmapImage(new Uri("afbeeldingen/" + imageNr + ".png", UriKind.Relative));
                images.Add(source);
            }
            int imageNrtemp = 1;

            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    kaartplaatje[row, col] = images.First();
                    kaartpleknummer[row, col] = imageNrtemp;
                    if (imageNrtemp == 8)
                    {
                        imageNrtemp = 0;
                    }
                    imageNrtemp++;
                    images.RemoveAt(0);
                }
            }
            Random r = new Random();
            for (int i = 0; i < rows; i++)
            {
                for (int b = 0; b < cols; b++)
                {
                    int randomIndexrow = r.Next(rows);
                    int randomIndexcol = r.Next(cols);
                    ImageSource temp = kaartplaatje[i, b];
                    int temp2 = kaartpleknummer[i, b];
                    kaartplaatje[i, b] = (kaartplaatje[randomIndexrow, randomIndexcol]);
                    kaartplaatje[randomIndexrow, randomIndexcol] = temp;
                    kaartpleknummer[i, b] = (kaartpleknummer[randomIndexrow, randomIndexcol]);
                    kaartpleknummer[randomIndexrow, randomIndexcol] = temp2;
                }
            }
        }

        //Cards turn on click
        /// <summary>
        /// Als cardclick wordt aangeroepen slaat hij de aangeklikt kaart op in kaartrow1,kaartcol1, 
        /// als het tweede kaartje wordt aangeklikt slaat hij het kaartje op in kaartrow2,kaartcol2.
        /// en als je het zelfde kaartje aan klik reset hij hem weer op een kaartje zodat je het niet kan matchen met hetzelfde kaartje.
        /// Als het twee kaarten zijn aangeklikt start hij de timer die de kaarten gaat controlleren.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CardClick(object sender, MouseButtonEventArgs e)
        {
            if (clicklimiet != 2)
            {

                if (kaartklik == 1)
                {
                    clicklimiet++;
                    kaartklik++;
                    Image card = (Image)sender;
                    int[] positie = (int[])card.Tag;
                    int row = positie[0];
                    int col = positie[1];
                    isClicked[row, col] = true;
                    card.Source = kaartplaatje[row, col];
                    kaartrow2 = row;
                    kaartcol2 = col;

                }
                if (kaartklik == 0)
                {
                    clicklimiet++;
                    kaartklik++;
                    Image card = (Image)sender;
                    int[] positie = (int[])card.Tag;
                    int row = positie[0];
                    int col = positie[1];
                    isClicked[row, col] = true;
                    card.Source = kaartplaatje[row, col];
                    kaartrow1 = row;
                    kaartcol1 = col;
                }
                if (kaartrow1 == kaartrow2 && kaartcol1 == kaartcol2)
                {
                    kaartklik--;
                    clicklimiet--;
                }
                if (kaartklik == 2)
                {
                    
                    kaartklik = 0;
                    timer.Start();
                }
           


            }
        }



        //Reset
        public void Reset()
        {
            grid.Children.Clear();
            AddImage();
        }
        /// <summary>
        /// Zodra dit wordt aan geroept, worden de kaartrow1,kaartcol1/kaartrow2,kaartcol2 omgezet naar ints a,b,c,d. 
        /// en die worden daarna gecontroleerd of ze dezelfde nummers hebben.
        /// zodra ze gematched worden wordt de achterkant gecontroleerd(met checkbackground), en wordt het click limiet weer gerest op 0.
        /// en als die gematched zijn wordt de speler die de beurt heeft krijgt +10 punten.
        /// aan het begin van het spel is er een variabele aan gemaakt die heet punten en is gelijk aan 0 gezet.
        /// elke keer als een speler +10 punten kreeg hield dat in dat ze een match hadden en kreeg de variabele ook +1.
        /// in totaal zijn er 8 verschillende kaarten dus als punten == 8 eindigt het spel en slaat met de method savescore de namen, score en datum van dat moment op.
        /// door if player1.GetScore() > player2.GetScore() dan heeft player 1 meer punten dan player 2 en dan heeft player 1 gewonnen. 
        /// door if player2.GetScore() > player1.GetScore() dan heeft player 1 meer punten dan player 2 en dan heeft player 2 gewonnen. 
        /// door if player1.GetScore() == player2.GetScore() dan is het gelijkspel. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void timer_Tick(object sender, EventArgs e)
        {
            isClicked[kaartrow1, kaartcol1] = false;
            isClicked[kaartrow2, kaartcol2] = false;
            int a = kaartrow1;
            int b = kaartcol1;
            int c = kaartrow2;
            int d = kaartcol2;
            if (kaartpleknummer[a, b] == (kaartpleknummer[c, d]))
            {
                kaartplaatje[a, b] = new BitmapImage(new Uri("afbeeldingen/transparant.png", UriKind.Relative));
                kaartplaatje[c, d] = new BitmapImage(new Uri("afbeeldingen/transparant.png", UriKind.Relative));
                achterkantplaatje[a, b].Source = kaartplaatje[a, b];
                achterkantplaatje[c, d].Source = kaartplaatje[c, d];
                clicklimiet = 0;

              // If a player has the turn and gets a match add points.
                if (player1.GetTurn() == true)
                {
                    punten++;
                    player1.SetScore(player1.GetScore() + 10);
                    
                }
                else
                {
                    punten++;
                    player2.SetScore(player2.GetScore() + 10);
                }
                // Update labels after scoring points
                UpdateLabels(player1, player2);
               
            }
            else
            {
                checkbackground(a, b);
                checkbackground(c, d);
                clicklimiet = 0;

                // Gives turn to other player when there is no match.
                if (player1.GetTurn() == true)
                {
                    
                    player1.SetTurn(false);
                    player2.SetTurn(true);
                }
                else
                {
                   
                    player1.SetTurn(true);
                    player2.SetTurn(false);
                }
               
            }
           
            if (punten == 8)
            {
                
                savescore();
                highscores win = new highscores();

                if (player1.GetScore() > player2.GetScore())
                {
                    MessageBox.Show(player1.GetName() + " heeft gewonnen");
                    
                }
                if (player2.GetScore() > player1.GetScore())
                {
                    MessageBox.Show(player2.GetName() + " heeft gewonnen");
                    
                }
                if (player1.GetScore() == player2.GetScore())
                {
                    MessageBox.Show("Gelijkspel");
                    

                }
                win.Show();
                this.Close();
            }
            timer.Stop();
            
            // Updates labels when there is no match.
            UpdateLabels(player1, player2);
        }
        /// <summary>
        /// 
        /// dit zorgt ervoor dat je de naam, score en de datum van als het gesaved is, van de players word opgeslagen.
        /// je maakt gebruik van de variabele path om naar de desktop te gaan en als het bestand highscore.sav nog niet bestaat
        /// word die dan aangemaakt en als hij wel al bestaat pakt hij hem gwn. Door true er achter te zetten word elke nieuwe informatie die in dat bestand word gezet
        /// niet overwritten. 
        /// </summary>
        void savescore()
        {
            
            using (var writer = new StreamWriter(path + "/highscore.sav", true)
        )
            {
                writer.WriteLine(player1.GetName() + ";" + player1.GetScore() + ";" + DateTime.Now);
                writer.WriteLine(player2.GetName() + ";" + player2.GetScore() + ";" + DateTime.Now);             
            }
        }

        

      
        /// <summary>
        /// hier worden de twee zelfde kaarten ge transformeerd naar een transparant plaatje.
        /// zodat het spel bord niet van groote veranderd.
        /// </summary>
        /// <param name="a">0,1,2,3</param>
        /// <param name="b">0,1,2,3</param>
        void checkbackground(int a, int b)
        {
            if (isClicked[a, b] == false)
            {
                achterkantplaatje[a, b].Source = new BitmapImage(new Uri("afbeeldingen/achterkant.png", UriKind.Relative));
            }
        }
        /// <summary>
        /// Save functie voor het aanmaken van een save.sav file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Savebutton(object sender, RoutedEventArgs e)
        {
            Save();
        }
        /// <summary>
        /// Nadat de functie Save(); wordt aan geroept wordt er een save.sav op het bureaublad aan gemaakt.
        /// Met een lijste waar de kaartpleknummer/kaartplaatje/scorebord/beurt opgeslagen.
        /// </summary>
        void Save()
        {
            using (var writer = new StreamWriter(path + "/save.sav"))
            {
                writer.WriteLine("kaartpleknummer");
                for (int i = 0; i < rows; i++)
                {
                    for (int b = 0; b < cols; b++)
                    {
                        writer.WriteLine(kaartpleknummer[i, b]);
                    }

                }
               
                writer.WriteLine("kaartplaatje");
                for (int c = 0; c < rows; c++)
                {
                    for (int d = 0; d < cols; d++)
                    {
                        writer.WriteLine(kaartplaatje[c, d]);
                    }

                }
                writer.WriteLine("scorebord");
                writer.WriteLine(player1.GetName());
                writer.WriteLine(player1.GetScore());
                writer.WriteLine(player2.GetName());
                writer.WriteLine(player2.GetScore());
                writer.WriteLine("beurt");
                writer.WriteLine(player1.GetTurn());
                

            }
        }
        /// <summary>
        /// Knop voor het laden van de save.sav
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Loadbutton(object sender, RoutedEventArgs e)
        {
                Load();
        }
        /// <summary>
        /// Als Load(); wordt aangeroept wordt er uit save.sav van het bureaublad de kaartpleknummer/kaartplaatje/scorebord/beurt, 
        /// uitgelezen en op de correct plekken gezet
        /// </summary>
        public void Load()
        {
            {
                    using (StreamReader reader = new StreamReader(path + "/save.sav"))
                    {

                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            if (line == ("kaartpleknummer"))
                            {
                                for (int row = 0; row < rows; row++)
                                {
                                    for (int col = 0; col < cols; col++)
                                    {
                                        line = reader.ReadLine();
                                        kaartpleknummer[row, col] = (Convert.ToInt32(line));
                                    }
                                }

                            }
                            if (line == ("kaartplaatje"))
                            {
                                for (int row = 0; row < rows; row++)
                                {
                                    for (int col = 0; col < cols; col++)
                                    {
                                        line = reader.ReadLine();
                                        if (line.StartsWith("pack://application:"))
                                        {
                                            line = line.Substring("pack://application:,,,/Memory Game;component/".Length);
                                        }
                                        kaartplaatje[row, col] = new BitmapImage(new Uri(line, UriKind.Relative));
                                        if (line == ("afbeeldingen/transparant.png"))
                                        {
                                            achterkantplaatje[row, col].Source = new BitmapImage(new Uri(line, UriKind.Relative));
                                        }
                                    }
                                }
                            }
                            string[] scorenamen = new string[4];
                            if (line == ("scorebord"))
                            {
                                for (int i = 0; i < 4; i++)
                                {
                                    line = reader.ReadLine();
                                    scorenamen[i] = line;
                                }
                                player1 = new Player(scorenamen[0], Convert.ToInt32(scorenamen[1]), true);
                                player2 = new Player(scorenamen[2], Convert.ToInt32(scorenamen[3]), true);
                            }
                            if (line == ("beurt"))
                            {
                                line = reader.ReadLine();
                                if (line == "False")
                                {
                                    player1.SetTurn(false);
                                    player2.SetTurn(true);
                                }
                                if (line == "True")
                                {
                                    player1.SetTurn(true);
                                    player2.SetTurn(false);
                                }
                                UpdateLabels(player1, player2);
                            }

                        }
                    
                }
            
            }

        }
        /// <summary>
        /// deze button zorgt ervoor dat het spel sluit
        /// </summary>
        /// <param name="sender">het object waarop geklikt kan worden</param>
        /// <param name="e"></param>
        private void btnclose(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// dit zorgt ervoor dat je weer naar de nameinput gaat om een nieuwe name te kunnen invoeren. Dit geeft je dus de mogelijkheid om het spel te restarten.
        /// </summary>
        /// <param name="sender">het object waar geklikt op kan worden</param>
        /// <param name="e"></param>
        private void btnrestart(object sender, RoutedEventArgs e)
        {

            NameInput win = new NameInput();
            player1.SetScore(0);
            player2.SetScore(0);
            win.Show();
            this.Close();




        }
        /// <summary>
        /// als je op deze button klikt zorgt dat ervoor dat je naar mainmenu gaat.
        /// </summary>
        /// <param name="sender">het object waar op geklikt kan worden</param>
        /// <param name="e"></param>
        private void btnhome(object sender, RoutedEventArgs e)
        {

            MainMenu win = new MainMenu();
            win.Show();
            this.Close();




        }



        
    }

}