﻿using memorygame;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace Memory_Game
{
    /// <summary>
    /// Interaction logic for NameInput.xaml
    /// </summary>
    public partial class NameInput : Window
    {
        
        public NameInput()
        {
            InitializeComponent();
        }
        private void btnhome(object sender, RoutedEventArgs e)
        {
            MainMenu mw = new MainMenu();
            mw.Show();
            this.Close();
        }
        private void btnclose(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        ///     The click event for the start button.
        ///     This checks if the inputs are not empty and starts the game.
        /// </summary>
        /// <param name="sender">The object that is being clicked on.</param>
        /// <param name="e">The event arguments.</param>
        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(InputP1.Text) || string.IsNullOrEmpty(InputP2.Text))
            {
                MessageBox.Show("Vul beide namen in om te kunnen spelen");
                return;
            }

            Player player1 = new Player(InputP1.Text, 0, true);
            Player player2 = new Player(InputP2.Text, 0, true);
            MainWindow win = new MainWindow(player1, player2,0);
            win.Show();
            this.Close();
        }



        /// <summary>
        ///     The click event for the back button.
        ///     This navigates to the main menu.
        /// </summary>
        /// <param name="sender">The object that is being clicked on.</param>
        /// <param name="args">The event arguments.</param>
        public void BackButtonClick(object sender, RoutedEventArgs args)
        {
            MainMenu win = new MainMenu();
            win.Show();
            this.Close();
        }
    }
}
